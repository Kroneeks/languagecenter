-- MariaDB dump 10.17  Distrib 10.4.8-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: language_courses
-- ------------------------------------------------------
-- Server version	10.4.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_number` (`group_number`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'1-de'),(2,'1-en'),(4,'1-es'),(6,'1-fr'),(5,'1-it'),(7,'1-ko'),(8,'1-ru'),(3,'1-zh');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'Немецкий язык'),(2,'Английский язык'),(3,'Китайский язык'),(4,'Испанский язык'),(5,'Итальянский язык'),(6,'Французский язык'),(7,'Корейский язык'),(8,'Русский язык');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Александров Фёдор Андреевич',23,1),(2,'Устинов Егор Вадимович',25,1),(3,'Крылов Николай Брониславович',19,1),(4,'Павлов Эрик Михайлович',31,1),(5,'Харитонов Юлий Артёмович',26,1),(6,'Шилов Устин Иванович',16,2),(7,'Королёв Станислав Вадимович',17,2),(8,'Коновалов Дмитрий Вадимович',20,2),(9,'Корнейчук Нестор Брониславович',18,2),(10,'Вишняков Никодим Виталиевич',24,2),(11,'Бачей Юрий Андреевич',28,2),(12,'Третьяков Динар Львович',35,3),(13,'Гончар Фёдор Анатолиевич',29,3),(14,'Кулибаба Юлий Васильевич',19,3),(15,'Назаров Харитон Григорьевич',42,4),(16,'Горбунов Павел Дмитриевич',32,4),(17,'Парасюк Прасковья Евгеньевна',22,5),(18,'Гущин Остап Андреевич',25,5),(19,'Галкина Жанна Юхимовна',18,6),(20,'Казакова Татьяна Вадимовна',24,6),(21,'Ершов Герман Сергеевич',16,6),(22,'Захаров Святослав Сергеевич',43,7),(23,'Русаков Николай Платонович',12,8),(24,'Степанова Олеся Сергеевна',10,8),(25,'Пахомова Жанна Артёмовна',16,8),(26,'Ларионов Платон Дмитриевич',8,8);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_subject` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_hours` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (1,'Немецкий язык',96),(2,'Английский язык',120),(3,'Китайский язык',128),(4,'Испанский язык',108),(5,'Итальянский язык',124),(6,'Французский язык',100),(7,'Корейский язык',104),(8,'Русский язык',92);
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_language`
--

DROP TABLE IF EXISTS `teacher_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_language` (
  `teacher_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`teacher_id`,`language_id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `language_fkid` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `teacher_fkid` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_language`
--

LOCK TABLES `teacher_language` WRITE;
/*!40000 ALTER TABLE `teacher_language` DISABLE KEYS */;
INSERT INTO `teacher_language` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8);
/*!40000 ALTER TABLE `teacher_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aducation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_experience` int(11) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `biography` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (1,'Афанасьева Наталья Ивановна','Филолог',1989,'1966-10-03','Здравствуйте. Меня зовут Наталья. Я - учитель немецкого языка. В начале своей карьеры я окончила Луганский государственный педагогический институт и получила диплом по специальности учитель русского языка и литературы. Но большое желание стать учителем немецкого не покидало меня. По прошествии времени, я завершила интенсивный курс обучения на факультете прикладной лингвистики в Берлине и получила Сертификат telc Deutsch, который дал мне право работать учителем немецкого языка в любой стране мира. В моем активе длительная работа в средней школе, работа репетитором и переводчиком, а также стажировка и далее 8-ми летний опыт проживания и работы в Берлине, 8 летний опыт общения с носителями немецого языка в Германии, их родной стране.'),(2,'Миронова Лариса Михайловна','Филолог',2013,'1992-05-13','Меня зовут Лариса. Я окончила факультет «английского языка и литературы» Ереванского государственного университета и имею дипломы бакалавра и магистра. За 6 лет моей практики преподавания английского языка я давала частные уроки школьникам, студентам и людям различных профессий. Более года я работала в многих языковых школах и имею опыт работы не только в сфере преподавания английского для всех (General English), но и в сфере технических, узкоспециализированных переводов и подготовки занятий английского, нацеленных для студентов конкретных специальностей.'),(3,'Чернов Вадим Сергеевич','Филолог',2012,'1985-04-01','Меня зовут Вадим. Более 7 лет преподаю китайский в своём родном университете, постоянно совершенствуя профессиональные навыки. Я окончил филологический факультет Переяслав-Хмельницкого государственного педагогического университета имени Григория Сковороды по специальности «преподаватель английского, китайского языков и зарубежной литературы» с дипломами бакалавра и магистра. В 2013 году получил кандидатскую степень в Национальном педагогическом университете имени М. П. Драгоманова. Преподавание стало моим хобби, и постоянный поиск интересных материалов для студентов приносит массу удовольствия. Стараюсь постоянно мотивировать студентов, делая уроки информативными и очень увлекательными. Готов всегда помочь идти к цели. Желаю удачи, и давайте учить китайский язык вместе!'),(4,'Фокина Мария Александровна','Филолог',2002,'1981-02-23','Меня зовут Мария. Я занимаюсь преподаванием испанского языка около 15 лет. Могу с уверенностью сказать, что учить язык сегодня можно действительно легко, весело, увлекательно и не принужденно.\n\nКак учитель, я очень хорошо понимаю волнение и дискомфорт ученика, которые периодически возникают в процессе обучения. Я всегда это чувствую и делаю все возможное, чтобы обстановка была комфортной, и студент всегда был уверен в моей поддержке и помощи.\n\nДля меня испанский язык стал действительно страстным увлечением: его грамматика захватывает, многозначные слова в полной мере показывают бесконечность и увлекательность процесса, а его звучание всегда радует слух.'),(5,'Рогова Виктория Алексеевна','Филолог',2010,'1987-11-15','Моя история — это история от ненависти до любви к итальянскому языку. Будучи школьницей, абсолютно не понимала грамматику, фонетику итальянского языка и многое другое. Но неизведанный, тайный для меня итальянский язык манил. Кроме того, в путешествиях приходилось общаться, и язык мне был действительно нужен. Таким образом, я взяла себя в руки, запаслась терпением и приложила все усилия, чтобы добиться результата. Мне, как и каждому, доводилось ошибаться, сомневаться в своих силах, преодолевать языковой барьер. Успешно пройдя стажировку в Ирландии, поборола все свои страхи и начала активно повышать свой уровень владения языком и свободно общаться с носителями.'),(6,'Смирнова Наталья Ивановна','Филолог',2014,'1992-07-19','Меня зовут Анна, и я являюсь преподавателем французского языка. С самого детства я была окружена и очарована французским языком, но и во взрослой жизни ни одно, ни другое не изменилось: я регулярно общаюсь с носителями языка, как в разговорной, так и в профессиональной сфере, а также продолжаю увлеченно совершенствовать свои знания и навыки. В ходе обучения сначала в России, а затем и во Франции я понял, что подача материала и то, насколько преподаватель любит свое дело, оказывается в итоге едва ли не основным элементом всего процесса. Так, обучение должно приносить чувство вдохновения и хотя бы немного удовольствия. Именно на это я и опираюсь при планировании и проведении своих уроков, а собственная любовь к французскому языку и культуре в этом только помогает.'),(7,'Дементьева Татьяна Ивановна','Филолог',2006,'1987-04-04','Меня зовут Татьяна. Я окончила факультет романо-германской филологии Тюменского государственного университета. После университета поехала в Корею и прожила там три года. На этом мои путешествия не закончились, и я отправилась в Бразилию, где прожила 1,5 года и за короткое время смогла освоить еще один язык — португальский.\n\nКорейский язык преподаю около 10 лет. Работала как в лингвистических центрах, школах, так и проводила уроки онлайн. Люблю работать как с детьми, так и со взрослыми. Для меня все мои ученики — как родная семья или одна большая команда, которая идет в одном направлении. И только вместе мы можем добиться уникальных результатов.'),(8,'Боброва Людмила Николаевна','Филолог',2006,'1990-08-09','Меня зовут Людмила. Я могу стать вашим гидом в мир русского языка. Занимаюсь частной практикой преподавания русского языка с 2008 года. Я с отличием закончила Оренбургский Государственный Университет. Но на самом деле, я начала обучать русскому еще обучаясь в старших классах в школе.\n\nЕсли у вас есть желание освоить русский, я с удовольствием вам могу в этом помочь. Всё возможно, когда есть желание.');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetable`
--

DROP TABLE IF EXISTS `timetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_time` datetime NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `subject_fkid_1` (`subject_id`),
  KEY `teacher_ibfk_1` (`teacher_id`),
  CONSTRAINT `subject_fkid_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
  CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`),
  CONSTRAINT `timetable_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetable`
--

LOCK TABLES `timetable` WRITE;
/*!40000 ALTER TABLE `timetable` DISABLE KEYS */;
INSERT INTO `timetable` VALUES (1,'2019-10-30 18:00:00',1,1,1),(2,'2019-11-02 16:00:00',1,1,1),(3,'2019-11-05 18:00:00',1,1,1),(4,'2019-11-07 18:00:00',1,1,1),(5,'2019-11-09 16:00:00',1,1,1),(6,'2019-10-25 18:00:00',2,2,2),(7,'2019-10-27 15:00:00',2,2,2),(8,'2019-10-29 15:00:00',2,2,2),(9,'2019-10-30 18:00:00',2,2,2),(10,'2019-11-02 18:00:00',2,2,2),(11,'2019-11-01 18:00:00',3,3,3),(12,'2019-11-04 18:00:00',3,3,3),(13,'2019-11-07 17:00:00',3,3,3),(14,'2019-11-03 16:00:00',4,4,4),(15,'2019-11-06 14:00:00',4,4,4),(16,'2019-11-09 19:00:00',4,4,4),(17,'2019-10-30 18:30:00',5,5,5),(18,'2019-11-04 18:30:00',5,5,5),(19,'2019-11-08 15:00:00',5,5,5),(20,'2019-10-27 17:00:00',6,6,6),(21,'2019-10-30 18:00:00',6,6,6),(22,'2019-11-03 19:00:00',6,6,6),(23,'2019-11-05 13:00:00',7,7,7),(24,'2019-11-08 17:30:00',7,7,7),(25,'2019-11-12 14:00:00',7,7,7),(26,'2019-11-10 18:30:00',8,8,8),(27,'2019-11-14 18:00:00',8,8,8),(28,'2019-11-18 17:00:00',8,8,8);
/*!40000 ALTER TABLE `timetable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-17 22:13:32
