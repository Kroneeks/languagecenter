"use strict";

let activeElement = 0; // Номер активного элемента в списке преподавателей

document.addEventListener(`DOMContentLoaded`, () => {
  SetTeachersInformation();

  // Навешивание события клика на список преподавателей
  document.querySelectorAll(`.language-card`).forEach(element => {
    element.addEventListener(`click`, e => {
      showAndHideLanguageInfo(e);
    });
  });

  // Навешивание события клика на список таблиц
  document.querySelectorAll(`.table-menu-item`).forEach(element => {
    element.addEventListener(`click`, e => {
      showAndHideTables(e);
    });
  });

  // Навешивание события клика на каждую кнопку записи на курс у преподавателей
  document.querySelectorAll(`.btn-registration`).forEach(element => {
    element.addEventListener("click", e => {
      showPopupForm();
      changingActiveOptionInSelect(e);
      document.querySelector(
        `.popup-form_header`
      ).innerHTML = `Записаться на курс`;
      document.querySelector(`.popup-form_header`).setAttribute("data-type", 2);
    });
  });

  document
    .querySelector(`.popup-form_free-lesson`)
    .addEventListener("submit", async evt => {
      evt.preventDefault();

      const selectLang = document.getElementById(`languageSelect`);

      const name = document.querySelector(
        `.popup-form input[name='user_name']`
      );
      const email = document.querySelector(
        `.popup-form input[name='user_email']`
      );
      const phone = document.querySelector(
        `.popup-form input[name='user_tel']`
      );
      const req_type = document
        .querySelector(`.popup-form_header`)
        .getAttribute("data-type");
      const lang_id = selectLang.options[selectLang.selectedIndex].value;

      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/newreq");
      xhr.onreadystatechange = function() {
        if (xhr.readyState > 3 && xhr.status == 200) alert(xhr.responseText);
      };
      xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.send(
        `name=${name.value}&email=${email.value}&phone=${phone.value}&req_type=${req_type}&lang_id=${lang_id}`
      );
    });

  // Навешивание события клика на внешнюю область Popup формы
  document
    .querySelector(`.popup-form-container`)
    .addEventListener("click", function(e) {
      if (
        e.target.classList[0] === `popup-form-container` ||
        e.target.classList[1] === `btn-close`
      ) {
        const body = document.querySelector("body");
        body.style.overflowY = "scroll";
        body.style.marginRight = "0px";
        document.querySelector(`.popup-form-container`).style.display = "none";
      }
    });

  // Навешивание события клика на кнопку записи на бесплатный курс
  document.querySelector(`.btn-free-lesson`).addEventListener("click", () => {
    showPopupForm();
    document.querySelector(`.popup-form_free-lesson`).action = `/freelesson`;
    document.querySelector(`.popup-form_header`).innerHTML = `Бесплатный урок`;
    document.querySelector(`.popup-form_header`).setAttribute("data-type", 1);
  });
});

// Заполнение инфы о преподавателях
function SetTeachersInformation() {
  // Очистка инфа
  sessionStorage.clear();

  loadJSON(response => {
    changeLanguageCardDOM(JSON.parse(response));
  });
}

// Получение инфы из JSON файла
function loadJSON(callback) {
  const xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "../teachers.json", true);
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == 200) {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

// Внесение инфы в блоки Имени, Образование, Возраста и Стажа
// информацией из JSON объекта
function changeLanguageCardDOM(teachersData) {
  const dt = new Date(),
    currentYear = dt.getFullYear();

  for (let i = 0, max = teachersData.table.length; i < max; i += 1) {
    document.querySelector(`.teacher-name-${i + 1}`).innerHTML =
      teachersData.table[i].full_name;

    document.querySelector(`.teacher-aducation-${i + 1}`).innerHTML =
      teachersData.table[i].aducation;

    document.querySelector(`.teacher-exp-${i + 1}`).innerHTML =
      teachersData.table[i].start_experience;

    document.querySelector(`.teacher-age-${i + 1}`).innerHTML =
      currentYear - teachersData.table[i].date_birth.slice(0, 4);

    document.querySelector(`.teacher-biography-${i + 1}`).innerHTML =
      teachersData.table[i].biography;
  }
}

// Открытие всплывающей формы
function showPopupForm() {
  const body = document.querySelector("body");
  body.style.overflow = "hidden";
  body.style.marginRight = "15px";
  document.querySelector(`.popup-form-container`).style.display = "block";
}

// Изменение выбранного элемента в Select'е (Английский язык, Немецкий и т.д)
function changingActiveOptionInSelect(e) {
  const indexActiveLanguageBtn = +e.target.classList[2].replace(
    `btn-registration-`,
    ""
  );
  const selectItem = document.getElementById(`languageSelect`);
  selectItem.options.selectedIndex = indexActiveLanguageBtn - 1;
}

// Получение таблицы с сервера
function loadTable(callback, table) {
  const xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", `/${table}`, true);
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == 200) {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

// Создание таблиц в html'е
function createTables(name, obj, countColumns, listArguments) {
  const table = document.querySelector(`.${name}-table`).childNodes[1];

  let nodeTr = document.createElement("tr"),
    nodeTd = document.createElement("td"),
    tdText;

  // Создание строк
  for (let i = 0, max = obj.length; i < max; i += 1) {
    table.append(nodeTr);
    // Создание стобцов и их заполнение
    for (let j = 0; j < countColumns; j += 1) {
      switch (j) {
        case 0:
          tdText =
            name === "time"
              ? new Date(obj[i][listArguments[0]])
              : obj[i][listArguments[0]];
          break;
        case 1:
          tdText = obj[i][listArguments[1]];
          break;
        case 2:
          tdText = obj[i][listArguments[2]];
          break;
        case 3:
          tdText = obj[i][listArguments[3]];
          break;
      }
      nodeTd.innerHTML = tdText;
      nodeTr.append(nodeTd);
      nodeTd = document.createElement("td");
    }
    nodeTr = document.createElement("tr");
  }
}

// Вывод и сокрытие карточек с инфой о преподах
async function showAndHideLanguageInfo(event) {
  const currentActiveElement = event.currentTarget.classList[1].replace(
    `card-`,
    ``
  );

  // Скрывать элемент если он выбран и по нему нажимают еще раз
  if (activeElement !== 0 && currentActiveElement !== activeElement) {
    const cardDescription = document.querySelector(
      `.card-description-${activeElement}`
    );

    cardDescription.style.height = `0px`;
    cardDescription.style.width = `0px`;
    await sleep(300);
  }
  activeElement = currentActiveElement;

  const cardDescription = document.querySelector(
    `.card-description-${activeElement}`
  );
  const cardDescriptionContainer = document.querySelector(
    `.list-languages-description`
  );

  // Непосредственно показ и сокрытие элемента
  if (cardDescription.style.height === `600px`) {
    cardDescription.style.height = `0px`;
    cardDescription.style.width = `0px`;
    cardDescriptionContainer.style.width = `0px`;
    cardDescriptionContainer.style.height = `0px`;
  } else {
    cardDescription.style.height = `600px`;
    cardDescription.style.width = `100%`;
    cardDescriptionContainer.style.width = `100%`;
    cardDescriptionContainer.style.height = `600px`;
  }
}

// Вывод и сокрытие таблиц
function showAndHideTables(event) {
  const targetElement = event.target.classList[1].replace(`table-menu-`, ``);
  let activeTable = sessionStorage.getItem(`activeTable`);

  if (activeTable === targetElement) {
    // Скрывать таблицу если она выбрана и по ней жмут еще раз
    document.querySelector(`.${activeTable}-table`).style.display = `none`;
    document
      .querySelector(`.table-menu-${activeTable}`)
      .classList.toggle(`active`);
    document.querySelector(
      `.tables-content`
    ).style.backgroundImage = `url(data:image/gif;base64,R0lGODlhCgAIAIABAN3d3f///yH5BAEAAAEALAAAAAAKAAgAAAINjAOnyJv2oJOrVXrzKQA7)`;
    sessionStorage.removeItem(`activeTable`);
  } else {
    document
      .querySelector(`.${event.currentTarget.classList[1]}`)
      .classList.toggle(`active`);

    // Если уже выбрана другая таблица, закрыть её
    if (activeTable) {
      document.querySelector(`.${activeTable}-table`).style.display = `none`;
      document
        .querySelector(`.table-menu-${activeTable}`)
        .classList.toggle(`active`);
    }

    // Если инфа о таблице еще не бралась и она не известна
    if (!sessionStorage.getItem(`${targetElement}-req`)) {
      switch (targetElement) {
        case `students`:
          loadTable(response => {
            const json = JSON.parse(response);
            createTables(`students`, json, 3, [`full_name`, `age`, `group_id`]);
          }, "students");
          break;
        case `subjects`:
          loadTable(response => {
            const json = JSON.parse(response);
            createTables(`subjects`, json, 2, [
              `name_subject`,
              `subject_hours`
            ]);
          }, "subjects");
          break;
        case `groups`:
          loadTable(response => {
            const json = JSON.parse(response);
            createTables(`groups`, json, 1, [`group_number`]);
          }, "groups");
          break;
        case `time`:
          loadTable(response => {
            const json = JSON.parse(response);
            createTables(`time`, json, 4, [
              `class_time`,
              `group_id`,
              `subject_id`,
              `teacher_id`
            ]);
          }, "timetable");
          break;
        case `language`:
          loadTable(response => {
            const json = JSON.parse(response);
            createTables(`language`, json, 1, [`name`]);
          }, "languages");
          break;
      }
      // Добавление таблицы в список загружен с сервера,
      // для отмены загрузки повторно
      sessionStorage.setItem(`${targetElement}-req`, true);
    }
    document.querySelector(`.tables-content`).style.backgroundImage = `none`;

    // Открытие таблицы
    document.querySelector(`.${targetElement}-table`).style.display = `table`;
    sessionStorage.setItem(`activeTable`, `${targetElement}`);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
