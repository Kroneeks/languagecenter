# Language Center 

## Введение

1. Открыть консоль MySQL и последовательно ввести команды
```bash
use language_courses;

drop table requests;

create table request_types ( id int primary key auto_increment, name varchar(100) not null unique);

create table requests ( id int primary key auto_increment, name varchar(150) not null, email varchar(150) not null unique, phone varchar(150) not null unique, req_type int, lang_id int, foreign key (lang_id) references languages(id), foreign key (req_type) references request_types(id));

insert into request_types (name) values('Бесплатный урок');

insert into request_types (name) values('Запись на курс');
```

2. Открыть консоль *node* в директории **./langCourses** и запустить локальный сервер
```bash
npm i
node index.js
```

3. Пройти по адресу *http://localhost:5000*
