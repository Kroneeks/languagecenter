// Импорт модулей
const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const nodemailer = require("nodemailer");
const fs = require("fs");
const mysql = require("mysql");

const PORT = process.env.PORT || 5000;
const teachersInfo = { table: [] };

// Использование фреймфорка express для обработки запросов
const app = express();

// Подключение к статике сайта
app.use(express.static("client"));
app.use(express.json());

// Прасинг страницы для анализа запросов
app.use(bodyParser.urlencoded({ extended: true }));

// Создание подключения к БД (MySQL)
const con = mysql.createConnection({
  host: "localhost",
  database: "language_courses",
  user: "root",
  password: "5195"
});

// Обращение к подключенной БД
con.connect(function(err) {
  if (err) throw err;

  // Обработка запроса
  con.query("SELECT * FROM teachers", function(err, result, fields) {
    const currentYear = new Date().getYear() + 1900;
    if (err) throw err;

    for (let i = 0; i < result.length; i++) {
      result[i].start_experience = currentYear - result[i].start_experience;
      teachersInfo.table.push(result[i]);
    }

    // Запись таблицы в фалй 'teachers.json'
    fs.readFile("client/teachers.json", "utf8", function readFileCallback(
      err,
      data
    ) {
      if (err) throw err;
      const json = JSON.stringify(teachersInfo);
      fs.writeFile("client/teachers.json", json, "utf8", function(err) {
        if (err) throw err;
      });
    });
  });
});

// Ответ на запрос при загрузке страницы
app.get("/", function(req, res) {
  res.send(`Server working on PORT ${PORT}`);
});

// Ответ на зарос '/students'
app.get("/students", function(req, res) {
  con.query(`SELECT * FROM students`, function(err, result, fields) {
    if (err) throw err;
    res.end(JSON.stringify(result));
  });
});

// Ответ на зарос '/subjects'
app.get("/subjects", function(req, res) {
  con.query(`SELECT * FROM subjects`, function(err, result, fields) {
    if (err) throw err;
    res.end(JSON.stringify(result));
  });
});

// Ответ на зарос '/groups'
app.get("/groups", function(req, res) {
  con.query(`SELECT * FROM groups`, function(err, result, fields) {
    if (err) throw err;
    res.end(JSON.stringify(result));
  });
});

// Ответ на зарос '/timetable'
app.get("/timetable", function(req, res) {
  con.query(`SELECT * FROM timetable`, function(err, result, fields) {
    if (err) throw err;
    res.end(JSON.stringify(result));
  });
});

// Ответ на зарос '/languages'
app.get("/languages", function(req, res) {
  con.query(`SELECT * FROM languages`, function(err, result, fields) {
    if (err) throw err;
    res.end(JSON.stringify(result));
  });
});

app.post("/newreq", (req, res) => {
  const { name, email, phone, req_type, lang_id } = req.body;

  try {
    con.query(
      `insert into requests (name, email, phone, req_type, lang_id) values('${name}', '${email}', '${phone}', ${req_type}, ${lang_id})`
    );
    res.send("Запись произошла успешно");
  } catch (err) {
    res.send("Произошла ошибка");
  }
  res.end();
});

app.use(function(req, res) {
  if (req.body.user_name) {
    // Создание заголовка для письма
    const subject =
      req.originalUrl === `/freelesson`
        ? `Запись на бесплатный урок`
        : req.originalUrl === `/registration`
        ? `Регистрация на обучение`
        : req.originalUrl === `/consultation`
        ? `Консультация по обучению`
        : ``;

    // Создание сообщения
    const message = {
      from: req.body.user_email,
      to: `sashadoneze@gmail.com`,
      subject: `${subject}`,
      text: req.body.language
        ? `Имя: ${req.body.user_name}; Телефон: ${req.body.user_tel}; Язык: ${req.body.language}`
        : `Имя: ${req.body.user_name}; Телефон: ${req.body.user_tel}`
    };

    // Подключение к почтовому клиенту по smtp
    const transport = nodemailer.createTransport({
      host: "smtp.mailtrap.io",
      port: 2525,
      auth: {
        user: "0f888dc522a111",
        pass: "01ec058704999c"
      }
    });

    // Отправка сообщения
    transport.sendMail(message, function(err, info) {
      if (err) {
        console.log(err);
      }
    });
  }

  // Перенаправление на глувную страницу
  res.redirect("/");
});

// Создание сервера
http.createServer(app).listen(PORT);
